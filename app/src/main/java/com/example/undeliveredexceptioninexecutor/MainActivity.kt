package com.example.morethanoneexception

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.run.setOnClickListener { onRun() }
    }

    private fun onRun() {
        CoroutineScope(Dispatchers.Main).launch {
            executeHolderOperationAsync().await()
        }
    }

    private fun executeHolderOperationAsync(): Deferred<Unit> = GlobalScope.async {
        val holder = Holder()
        for (n in 0..10) {
            holder.executeOperation()
        }
    }

    class Holder {

        class InternalState {
            init {
                // Uncomment to ensure the `InternalState` constructor is visited more than once
//                Log.d("MainActivity", "InternalState::init $counter")
                if (counter == 0) {
                    Thread.sleep(500)
                }
                throw IllegalArgumentException("Thrown ${++counter} time(s)")
            }

            companion object {
                private var counter: Int = 0
            }
        }

        private val executor = Executors.newSingleThreadExecutor()
        private var state: InternalState? = null

        init {
            executor.execute {
                loadState()
            }
        }

        fun executeOperation() {
            executor.execute {
                loadState()
            }
        }

        private fun loadState() {
            if (state == null) {
                init()
            }
        }

        private fun init() {
            synchronized(this) {
                if (state == null) {
                    state = InternalState()
                }
            }
        }
    }
}
